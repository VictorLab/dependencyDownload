using System;
using System.Reflection;
using UnityEngine;

public class testUsingScript : MonoBehaviour
{
    void Start()
    {
        var assemblies = AppDomain.CurrentDomain.GetAssemblies();

        foreach (var assembly in assemblies)
        {
            // Only list assemblies that have the name of your package in their location
            if (assembly.Location.Contains("com.therapieland.dependencydownload"))
            {
                Debug.Log($"Loaded assembly: {assembly.FullName} from location: {assembly.Location}");

                foreach (var type in assembly.GetTypes())
                {
                    Debug.Log($"Type found in assembly: {type.FullName}");
                }
            }
        }
    }
}